import {Dialog, Loading} from 'quasar'
export function showErroMessage(errorMessage){
    Loading.hide()
    Dialog.create({
        title : 'Error',
        message : errorMessage
    })

}

