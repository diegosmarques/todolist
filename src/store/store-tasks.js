import Vue from 'vue'
import { uid, Notify } from 'quasar'
import {firebaseDb, firebaseAuth} from 'boot/firebase'
import { showErroMessage } from 'src/functions/functions-show-error-message'

const state = {
    /*I'm using an object instead of an array, because firebase only work with objetcs*/

    tasks:{
       
    },
    search:'',
    sort:'dueDate',
    tasksDownloaded:false

}

const mutations = {
    UPDATE_TASK(state, payload){
        Object.assign(state.tasks[payload.id],payload.updates)
    },
    DELETE_TASK(state, id){
        Vue.delete(state.tasks, id)
    },
    ADD_TASK(state,payload){
        Vue.set(state.tasks, payload.id, payload.task)

    },
    CLEAR_TASKS(state){ // This is to clear the object Tasks when the user logout
        state.tasks = {}
    },
    SET_SEARCH(state, value){
        state.search = value
    },
    SET_SORT(state, value){
        state.sort = value
    },
    SET_TASKDOWNLOADED(state, value){
        state.tasksDownloaded = value
    }
}

const actions = {
    updateTask({ dispatch }, payload){
        dispatch('firebaseUpdateTask', payload)
    },
    deleteTask({dispatch}, id){
        dispatch('firebaseDeleteTask', id)
    },
    addTask({dispatch}, task){
        let taskId = uid()
        let payload ={
            id: taskId,
            task: task
        }
        dispatch('firebaseAddTask',payload)
    },
    setSearch({commit}, value){
        commit('SET_SEARCH',value)
    },
    setSort({commit}, value){
        commit('SET_SORT',value)
    },
    firebaseReadData({ commit }) {
        let userId = firebaseAuth.currentUser.uid
        let userTasks = firebaseDb.ref("tasks/"+ userId)

        // Initial check for data 
        userTasks.once('value', snapshot=> {
            commit('SET_TASKDOWNLOADED', true)
        }, error =>{
            showErroMessage(error.message)
            this.$router.replace('/auth')
        })

        
        //child_added
        userTasks.on('child_added', snapshot =>{ // this is a methode from firebase to read data from the database and this
            let task = snapshot.val()

            let payload = {
                id: snapshot.key,
                task: task //  this second task is the var that i created on the ligne 81
            }

            commit('ADD_TASK', payload)
        })
        
        //child_changed using this to update the page when we have a change on the database

        userTasks.on('child_changed', snapshot =>{ 
            let task = snapshot.val()

            let payload = {
                id: snapshot.key,
                updates: task 
            }

            commit('UPDATE_TASK', payload)
        })

         //child_removed using this to update the page when we have a change on the database

        userTasks.on('child_removed', snapshot =>{ 
            let taskId = snapshot.key

            commit('DELETE_TASK', taskId)
        })
    },
    firebaseAddTask({}, payload){
        let userId = firebaseAuth.currentUser.uid
        let taskRef = firebaseDb.ref("tasks/"+ userId+"/"+payload.id)
        taskRef.set(payload.task, error=>{
            if(error){
                showErroMessage(error.message)
            }else{
                Notify.create('Task added')
            }
        })
    },
    firebaseUpdateTask({}, payload){
        let userId = firebaseAuth.currentUser.uid
        let taskRef = firebaseDb.ref("tasks/"+ userId+"/"+payload.id)
        taskRef.update(payload.updates, error=>{
            if(error){
                showErroMessage(error.message)
            }else{
                let keys = Object.keys(payload.updates)
                if(!(keys.includes('completed') && keys.length == 1))
                Notify.create('Task updated')
            }
        })
    },
    firebaseDeleteTask({}, taskId){
        let userId = firebaseAuth.currentUser.uid
        let taskRef = firebaseDb.ref("tasks/"+ userId+"/"+taskId)
        taskRef.remove(error=>{
            if(error){
                showErroMessage(error.message)
            }else{
                Notify.create('Task deleted')
            }
        })
    }  

}

const getters = {
    tasksSorted:(state)=>{
        let tasksSorted = {}
        let keysOrdered = Object.keys(state.tasks)

        keysOrdered.sort((a,b)=>{ // We are using a and b to make a comparison between the property name of item ID1 and then ID2, and will do this comparion throughtout the whole array
            let taskAProp = state.tasks[a][state.sort].toLowerCase()
            let taskBProp = state.tasks[b][state.sort].toLowerCase()

            if(taskAProp > taskBProp) return 1
            else if( taskAProp < taskBProp) return -1
            else return 0
            
        })
        keysOrdered.forEach((key)=>{
            tasksSorted[key] = state.tasks[key]
        })
        return tasksSorted
    },
    tasksFiltered:(state, getters)=>{
        let taskSorted = getters.tasksSorted
        let tasksFiltered = {}
        if(state.search){
            Object.keys(taskSorted).forEach(function(key){
                let task = taskSorted[key],
                taskNameToLowerCase = task.name.toLowerCase(),
                searchLowerCase = state.search.toLowerCase()
                if(taskNameToLowerCase.includes(searchLowerCase)){
                    tasksFiltered[key]= task
                }
            });
            return tasksFiltered
        }
        return taskSorted
    },
    tasksTodo:(state, getters)=>{
        let tasksFiltered = getters.tasksFiltered
        let tasks={}
            Object.keys(tasksFiltered).forEach(function(key){
                let task = tasksFiltered[key]
                if(!task.completed){
                    tasks[key]= task
                }
            });
        return tasks
    },
    tasksCompleted:(state, getters)=>{
        let tasksFiltered = getters.tasksFiltered
        let tasks={}
            Object.keys(tasksFiltered).forEach(function(key){
                let task = tasksFiltered[key]
                if(task.completed){
                    tasks[key]= task
                }
            });
        return tasks
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}