import { LocalStorage } from 'quasar'
const state = {
    settings:
    {
        show12HourTimeFormart:false,
        showTasksInOneList: false

    }

}

const mutations = {
    SET_SHOW_12_HOUR_TIME_FORMAT(state, value){
        state.settings.show12HourTimeFormart = value
    },
    SET_SHOW_TASK_IN_ONE_LIST(state, value){
        state.settings.showTasksInOneList = value
    },
    SET_SETTINGS(state, settings){
        Object.assign(state.settings, settings)
    }
}

const actions = {
    setShow12HourTimeFormart({ commit, dispatch }, value){ // dispatch is used to pass an action in this method
        commit('SET_SHOW_12_HOUR_TIME_FORMAT', value)
        dispatch('saveSettings')
    },
    setShowTasksInOneList({ commit, dispatch }, value){
        commit('SET_SHOW_TASK_IN_ONE_LIST', value)
        dispatch('saveSettings')
    },
    saveSettings({ state }){
        LocalStorage.set('settings', state.settings)
        
    },
    getSettings({ commit }){
        let settings =  LocalStorage.getItem('settings')
            if(settings){
                commit('SET_SETTINGS',settings)
            }
    }

}

const getters = {
    settings: state =>{
        return state.settings
    }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}