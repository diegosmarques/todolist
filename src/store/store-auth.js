import { firebaseAuth } from 'boot/firebase'
import { LocalStorage, Loading } from 'quasar'
import {showErroMessage} from 'src/functions/functions-show-error-message'

const state = {
    loggedIn:false

}

const mutations = {
    setLoggedIn(state, value){
        state.loggedIn = value
    }
}

const actions = {
    registerUser({}, payload){
        Loading.show()
        firebaseAuth.createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response =>{
            console.log('response : ', response);
        })
        .catch(error => {
            showErroMessage(error.message);
        })
    },
    loginUser({}, payload){
        Loading.show()
        firebaseAuth.signInWithEmailAndPassword(payload.email, payload.password)
        .then(response =>{
            
        })
        .catch(error => {
            showErroMessage(error.message);
        })
    },
    logoutUser(){
        firebaseAuth.signOut()
    },
    handleAuthStateChange({ commit, dispatch}){
        firebaseAuth.onAuthStateChanged(user => {
            Loading.hide()
            if (user) {
                commit('setLoggedIn', true)
                LocalStorage.set('loggedIn', true)
                this.$router.push('/').catch(err=> {})
                dispatch('tasks/firebaseReadData', null, {root: true}) // when we want to use an action from another store we can use dispatch with 3 parameters, 1 name of the store with / and then the action, 2 payload, one object
            }
            else{
                commit('tasks/CLEAR_TASKS', null, { root: true})
                commit('tasks/SET_TASKDOWNLOADED', false, { root: true})
                commit('setLoggedIn', false)
                LocalStorage.set('loggedIn', false)
                this.$router.replace('/auth').catch(err=> {}) // use replace so the user can't go back with and it is necessary to login again
            }
          });

    }
}

const getters = {
   
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters

}